const firebaseConfig = {
    apiKey: "AIzaSyCABSoFmiwmg3TLPwVIXi1pmCgqfNtKD04",
    authDomain: "messenger-demo-556e8.firebaseapp.com",
    databaseURL: "https://messenger-demo-556e8.firebaseio.com",
    projectId: "messenger-demo-556e8",
    storageBucket: "",
    messagingSenderId: "170437209142",
    appId: "1:170437209142:web:c91e80734608339a10fc97"
};
firebase.initializeApp(firebaseConfig);


$(document).ready(function () {

    $('#register-phone').submit(function (event) {
        window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
        const appVerifier = window.recaptchaVerifier;
        event.preventDefault();
        let phoneNumber = $('input[name="phoneNumber"]').val();
        if (phoneNumber.substring(0, 1) == '0')
            phoneNumber = '+84' + phoneNumber.substring(1, phoneNumber.length);

        firebase.auth().signInWithPhoneNumber(phoneNumber, appVerifier)
            .then(function (confirmationResult) {
                window.confirmationResult = confirmationResult;
                const firstName = $('input[name="firstName"]').val();
                const lastName = $('input[name="lastName"]').val();
                $('#register-phone-1').remove();
                $('#register-phone-2').css('display', 'block');
                $('#phone-number-verify').submit(function (event) {
                    event.preventDefault();
                    const code = $('input[name="verficationCode"]').val();
                    confirmationResult.confirm(code).then(function (result) {
                        const user = result.user;
                        firebase.auth().currentUser.getIdToken(true).then(function (idToken) {
                            const dataRequest = {
                                firstName,
                                lastName,
                                phoneNumber,
                                idToken
                            }
                            $.ajax({
                                type: 'post',
                                url: '/registerphone',
                                data: dataRequest,
                                success: () => {
                                    location.href = "/conversations/home";
                                }
                            });
                        })
                    }).catch(function (error) {
                        alert('Verify code failed');
                    });
                });
            })
            .catch(function (error) {
                alert('Send verify code failed');
            });
    });


    $('#login-phone-number').submit(function (event) {
        window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
        const appVerifier = window.recaptchaVerifier;
        event.preventDefault();
        let phoneNumber = $('input[name="phoneNumber"]').val();
        if (phoneNumber.substring(0, 1) == '0')
            phoneNumber = '+84' + phoneNumber.substring(1, phoneNumber.length);

        firebase.auth().signInWithPhoneNumber(phoneNumber, appVerifier)
            .then(function (confirmationResult) {
                window.confirmationResult = confirmationResult;
                $('#login-phone-1').remove();
                $('#login-phone-2').css('display', 'block');
                $('#phone-number-verify').submit(function (event) {
                    event.preventDefault();
                    const code = $('input[name="verficationCode"]').val();

                    confirmationResult.confirm(code).then(function (result) {
                        const user = result.user;
                        console.log("TCL: user", user)
                        firebase.auth().currentUser.getIdToken(true).then(function (idToken) {
                            const dataRequest = {
                                idToken,
                                phoneNumber
                            }
                            $.ajax({
                                type: 'post',
                                url: '/loginphone',
                                data: dataRequest,
                                success: (data) => {
                                    window.location.replace("/conversations/home");
                                }
                            });
                        })
                    }).catch(function (error) {
                        alert('Verify code failed');
                    });
                });
            }).catch(function (error) {
                alert('Error sending code');
            });
    });


    $('#registeremail').submit(function (event) {
        window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
        const appVerifier = window.recaptchaVerifier;
        event.preventDefault();
        const firstName = $('input[name="firstName"]').val();
        const lastName = $('input[name="lastName"]').val();
        const email = $('input[name="email"]').val();
        const password = $('input[name="password"]').val();

        firebase.auth().createUserWithEmailAndPassword(email, password).then(function () {
            const user = firebase.auth().currentUser;
            const dataRequest = {
                firstName,
                lastName,
                email,
                password
            };
            if (user) {
                user.sendEmailVerification().then(function () {
                    $.ajax({
                        type: 'post',
                        url: '/registeremail',
                        data: dataRequest
                    });
                    alert('Please check your email to verify your account');
                    window.location.replace("/login");
                });
            }
        }).catch(function (error) {
            alert(error);
        });
    });


    $('#login').submit(function (event) {
        event.preventDefault();
        const email = $('input[name="email"]').val();
        const password = $('input[name="password"]').val();
        const dataRequest = {
            email,
            password
        };
        firebase.auth().signInWithEmailAndPassword(email, password).then(function () {
            const user = firebase.auth().currentUser;
            if (user.emailVerified) {
                $.ajax({
                    type: 'post',
                    url: '/login',
                    data: dataRequest,
                    success: (result) => {
                        if (result == 'false') {
                            return alert('The password is invalid or the user does not have a password.');
                        }
                        else {
                            window.location.replace("/conversations/home");
                        }
                    }
                });
            }
            else {
                alert('Please verify email');
            }
        }).catch(function (error) {
            alert(error);
        });
    });

    $('#logout').click(function (event) {
        event.preventDefault();
        firebase.auth().signOut().then(function () {
            window.location.href = "/logout";
        }).catch(function (error) {
        });
    });

    $('#edit-profile').submit(function (event) {
        event.preventDefault();
        const firstName = $('input[name="firstName"]').val();
        const lastName = $('input[name="lastName"]').val();
        const city = $('input[name="city"]').val();
        let phoneNumber = $('input[name="phoneNumber"]').val();
        const email = $('input[name="email"]').val();
        const password = $('input[name="password"]').val();
        const describe = $('input[name="describe"]').val();

        if (phoneNumber.substring(0, 1) == '0') {
            phoneNumber = '+84' + phoneNumber.substring(1, phoneNumber.length);
        }

        const dataRequest = {
            firstName,
            lastName,
            city,
            phoneNumber,
            email,
            password,
            describe,
        }
        $.ajax({
            type: 'post',
            url: '/editprofile',
            data: dataRequest,
            success: (error) => {
                if (error != 'true') {
                    alert('Update successful');
                }
                else {
                    alert('Phone number or email already exists');
                }
            }
        });
    });

    $('#upload-avatar').submit((event) => {
        event.preventDefault();
        const formData = new FormData();
        formData.append('avatar', $('#avatar')[0].files[0]);
        console.log("TCL: formData", formData)

        $.ajax({
            url: '/uploadavatar',
            data: formData,
            enctype: "multipart/form-data",
            processData: false,
            type: 'POST',
        });
    })

    $('#add-friend').submit(function (event) {
        event.preventDefault();
        const link = $('input[name="email-phone"]').val();
        dataRequest = {
            link
        }
        $.ajax({
            type: 'post',
            url: '/addfriend',
            data: dataRequest,
            success: (error) => {
                if (error == 'error') {
                    alert('Failed to send invitation');
                }
                else {
                    alert('Invitation has been sent');
                }
            }
        });
    })

    $('#accept-friend').click(function (event) {
        let idFriend = $('#accept-friend').attr('idFriend');
        $.ajax({
            type: 'POST',
            url: '/acceptfriend',
            data: {
                idFriend,
            },
            success: (status) => {
                alert('Now you are friends');
                $("li[idFriend=" + idFriend + "]").remove();
            }
        });
    })

    $('#deny-friend').click(function (event) {
        event.preventDefault();
        const idFriend = $('#deny-friend').attr('idFriend');
        $.ajax({
            type: 'POST',
            url: '/denyfriend',
            data: {
                idFriend,
            },
            success: (status) => {
                alert('Deleted friends');
                $("li[idFriend=" + idFriend + "]").remove();
            }
        });
    })

    $('#addGroupUser').selectize({
        valueField: 'value',
        labelField: 'text',
        searchField: 'text',
        render: {
            item: (item, escape) => {
                return "<div class='item'><img class='rounded-circle m-1 avatar-add-user-gr-selected' src='/dist/images/man_avatar3.jpg'>" + escape(item.text) + "</div>"
            },

            option: (item, escape) => {
                return "<div><img class='rounded-circle m-1 avatar-add-user-gr' src='/dist/images/man_avatar3.jpg'><strong>" + escape(item.text) + "</strong></div>";
            }
        }
    });

    $('#addGroupUser-selectized').on('keyup', function () {

        const fullName = $(this).val();
        let selectize = $('#addGroupUser')[0].selectize;
        selectize.clearOptions();
        selectize.refreshOptions();
        if (fullName != '') {
            const userInfo = {
                fullName,
            }
            $.ajax({
                type: 'POST',
                url: '/get-user-info',
                data: userInfo,
                success: (data) => {
                    data.forEach((data) => {
                        selectize.addOption({ value: data.id, text: data.fullName });
                        selectize.refreshOptions();
                    });
                }
            });
        }
    })

    $('#createGroup').submit(function (event) {
        event.preventDefault();
        const groupName = $('input[name="groupName"]').val();
        const description = $('textarea[name="description"]').val();
        let users = $('#addGroupUser').val();
        users = users.map(Number);
        if (groupName == '') {
            return alert('Plz input name group')
        }
        if (users.length < 2) {
            return alert('The number of members must be greater than 3')
        }
        users = JSON.stringify(users);
        $.ajax({
            type: 'POST',
            url: '/group-add',
            data: {
                users,
                groupName,
                description,
            },
            success: function (User) {
                $('#newGroup').modal('hide');
                $('#addGroupUser')[0].selectize.clear();
                $('#createGroup')[0].reset();
                $('.user-id').each(function () {
                    $('.user-id').remove();
                });
                location.href = "/conversations/home";
            },
        });
    });

    const socket = io.connect('/conversations');

    $(document).ready(function () {
        let idRoom = window.location.pathname;
        const idUser = $('#content-message').attr('data-idSender');
        idRoom = idRoom.substring(idRoom.indexOf('/conversations') + 15);
        if (idRoom && idRoom != 'home') {
            const nameRoom = $('.list-group li[data-_id = ' + idRoom + '] .users-list-body h5').text();
            $('.chat-header-user div h5').replaceWith('<h5>' + nameRoom + '</h5>')
            $.ajax({
                type: 'post',
                url: '/get-mess-room',
                data: { idRoom },
                success: (data) => {
                    $('.chat-header-user div h5').replaceWith('<h5>' + data[0].conversationId.groupName + '</h5>')
                    data.forEach((data) => {
                        if (data.idSender != idUser) {
                            appendFriendMsg(data)
                        }
                        else {
                            appendUserMsg(data)
                        }
                    })
                }
            });
            socket.emit('joinRoom', idRoom, idUser);
            $('#content-message').attr('data-_idRoom', () => idRoom);
        }
    });

    $('.list-group').on('click', '.list-group-item', function (event) {
        event.preventDefault();
        const idRoom = $(this).attr('data-_id');
        const nameRoom = $('.users-list-body h5', this).text();
        const idUser = $('#content-message').attr('data-idSender');
        const oldRoom = $('#content-message').attr('data-_idRoom');
        $('.messages .message-item').each(() => {
            $('.messages .message-item').remove();
        })
        $('.chat-header-user div h5').replaceWith('<h5>' + nameRoom + '</h5>')
        $.ajax({
            type: 'post',
            url: '/get-mess-room',
            data: { idRoom },
            success: (data) => {
                data.forEach((data) => {
                    if (data.idSender != idUser) {
                        appendFriendMsg(data)
                    }
                    else {
                        appendUserMsg(data)
                    }
                })
            }
        });
        $('#content-message').attr('data-_idRoom', () => idRoom);
        socket.emit('leaveRoom', oldRoom);
        socket.emit('joinRoom', idRoom);
        history.pushState('', '', "/conversations/" + idRoom + "");
    })

    $('#message-send').submit(function (event) {
        event.preventDefault();
        const contentMessage = $('input[name="content-message"]').val();
        const idRoom = $('#content-message').attr('data-_idRoom');
        const idSender = $('#content-message').attr('data-idSender');
        const nameSender = $('#content-message').attr('data-nameSender')

        infoMess = {
            idRoom,
            idSender,
            nameSender,
            contentMessage,
        }
        $.ajax({
            type: 'post',
            url: '/save-mess',
            data: infoMess,
        });

    })

    socket.on('show-message', (data) => {
        const idUser = $('#content-message').attr('data-idSender');
        if (data.idSender != idUser) {
            appendFriendMsg(data)
        }
    });

    socket.on('reload-chatbar', (data) => {
        const idUser = $('#content-message').attr('data-idSender');
        console.log('aaaaaaaaaaaaaa');
        let itemListGroup;
        if (data.idSender != idUser) {
            $('li[ data-_id = ' + data.idRoom + '] .users-list-body p').html('<p><strong>' + data.nameSender + ': </strong>' + data.contentMessage + '</p>');
        }
        else {
            $('li[ data-_id = ' + data.idRoom + '] .users-list-body p').html('<p><strong>You: </strong>' + data.contentMessage + '</p>');
        }
        $('.list-group li').first().before($('li[ data-_id = ' + data.idRoom + ']'))
    })

    function appendFriendMsg(data) {
        const msgContainer = '<div class="message-item m-0">' +
            '<div class="message-action">' + data.nameSender + '</div></div>' +
            '<div class="message-item">' +
            '<div class="message-content" id="msg-received">' + data.contentMessage + '</div>' +
            '<div class="message-action">Pm 14:20</div>' +
            '<div class="l-arrow"></div>' +
            '</div>'

        $('.messages').append(msgContainer);
        const chat_body = $('.layout .content .chat .chat-body');
        chat_body.scrollTop(chat_body.get(0).scrollHeight, -1).niceScroll({
            cursorcolor: 'rgba(66, 66, 66, 0.20)',
            cursorwidth: "4px",
            cursorborder: '0px'
        }).resize()
    }

    function appendUserMsg(data) {
        const msgContainer = '<div class="message-item outgoing-message">' +
            '<div class="message-content" id="msg-received">' + data.contentMessage + '</div>' +
            '<div class="message-action">Pm 14:20</div>' +
            '<div class="l-arrow"></div>' +
            '</div>'

        $('.messages').append(msgContainer);
        const chat_body = $('.layout .content .chat .chat-body');
        chat_body.scrollTop(chat_body.get(0).scrollHeight, -1).niceScroll({
            cursorcolor: 'rgba(66, 66, 66, 0.20)',
            cursorwidth: "4px",
            cursorborder: '0px'
        }).resize()
    }

});
