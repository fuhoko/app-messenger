import mongoose from 'mongoose';

const { Schema } = mongoose;

const conversationSchema = new Schema({
    groupName: String,
    member: Array,
    user_1: String,
    user_2: String,
    lastContentMessage: String,
    lastIdSender: Number,
    lastNameSender: String,
    },
    {
        timestamps: true,
    });

export default mongoose.model('Conversation', conversationSchema);
