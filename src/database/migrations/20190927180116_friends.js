exports.up = (knex) => knex.schema.createTable('friends', (table) => {
    table.increments('id').primary();
    table.integer('user_1').unsigned();
    table.integer('user_2').unsigned();
    table.integer('id_receiver').unsigned();
    table.string('status');
    table.timestamp('createdAt').defaultTo(knex.fn.now());
    table.timestamp('updatedAt').defaultTo(knex.fn.now());
  });
exports.down = (knex) => knex.schema.dropTable('friends');
