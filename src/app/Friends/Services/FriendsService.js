import friendRepository from '../Repositories/FriendsRepository';

class FriendsService {
  static service;

  constructor() {
    this.friendRepository = friendRepository.getfriendRepository();
  }

  static getService() {
    if (!this.service) {
      this.service = new this();
    }
    return this.service;
  }

  async checkFriend(data, idUser) {
    return await this.friendRepository.checkFriend(data, idUser);
  }
  
  async addfriend(infoFriend, idUser) {
    return await this.friendRepository.addfriend(infoFriend, idUser);
  }

  async friendRequest(idUser) {
    return await this.friendRepository.friendRequest(idUser);
  }

  async showFriend(idUser) {
    return await this.friendRepository.showFriend(idUser);
  }

  async acceptFriend(data, idUser) {
    return await this.friendRepository.acceptFriend(data, idUser);
  }

  async denyFriend(data, idUser) {
    return await this.friendRepository.denyFriend(data, idUser);
  }

}
export default FriendsService;
