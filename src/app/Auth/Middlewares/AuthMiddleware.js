import Service from '../Services/AuthService';

class AuthMiddleware { 
    async verifyCode(req, res, next) {
        const data = req.body;
        await Service.getService().verifyCode(data, req);
        return next();
    }

    verifyAuthentication(req, res, next) {
        return Service.getService().verifyAuthentication(req, res, next);
    }

    verifyNotAuthtication(req, res, next) {
        return Service.getService().verifyNotAuthtication(req, res, next);
    }
}

export default AuthMiddleware;
