import multer from 'multer';
import path from 'path';

const Storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'public/dist/images/avatar')
    },

    filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
    }

});

const uploadImages = multer({ storage: Storage }).single('avatar');

export default { uploadImages };
