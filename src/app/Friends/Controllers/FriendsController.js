import BaseController from '../../../infrastructure/Controllers/BaseController';
import authService from '../../Auth/Services/AuthService';
import friendsService from '../Services/FriendsService';
import conversationService from '../../Conversation/Services/ConversationService';

class FriendsController extends BaseController {
  constructor() {
    super();
    this.authService = authService.getService();
    this.friendsService = friendsService.getService();
    this.conversationService = conversationService.getService();
  }

  async addfriend(req, res) {
    const data = req.body;
    const idUser = req.session.user.id;
    const isEmail = /[A-Z0-9._%+-]+@[A-Z0-9-]+.+.[A-Z]{2,4}/igm;
    const isPhoneNumber = /[0-9]/;
    let infoFriend;

    if (isEmail.test(data.link)) {
      infoFriend = await this.authService.findUserByEmail(data);
    } else if (isPhoneNumber.test(data.link)) {
      if (data.link.substring(0, 1) == '0') {
        data.link = '+84' + data.link.substring(1, data.link.length);
      }
      infoFriend = await this.authService.findUserByPhone(data);
    } else {
      res.json('error');
    }

    if (infoFriend && (idUser != infoFriend.id)) {
      const checkFriend = await this.friendsService.checkFriend(infoFriend.id, idUser)
      if (checkFriend) {
        return res.json('error');
      }

      await this.friendsService.addfriend(infoFriend, idUser);
      return res.json('true');
    }
    res.json('error');
  }

  async friendRequest(req, res) {
    const idUser = req.session.user.id;
    const listRequest = await this.friendsService.friendRequest(idUser);
    return res.render('app/conversation/index', { listRequest, user: req.session.user });
  }

  async showFriend(req, res, next) {
    const idUser = req.session.user.id;
    const listFriend = await this.friendsService.showFriend(idUser);
    res.locals.listFriend = listFriend;
    next();
  }

  async acceptFriend(req, res) {
    let data = req.body;
    const idUser = req.session.user.id;
    data.idFriend = Number(data.idFriend);
    const memberGroup = [data.idFriend, idUser];

    await this.friendsService.acceptFriend(data, idUser);

    const checkRoomChat = await this.conversationService.RoomChat(memberGroup);
    console.log("TCL: FriendsController -> acceptFriend -> checkRoomChat", checkRoomChat)
    if (checkRoomChat.length == 0) {
      const infoFriend = await this.authService.findUserById(data);
      const infoUser = await this.authService.findUserById(req.session.user);
      await this.conversationService.addFriendChat(memberGroup, infoFriend, infoUser);
    }
    res.json('success');
  }

  async denyFriend(req, res) {
    const data = req.body;
    const idUser = req.session.user.id;
    await this.friendsService.denyFriend(data, idUser);
    res.json('success');
  }

}

export default FriendsController;
