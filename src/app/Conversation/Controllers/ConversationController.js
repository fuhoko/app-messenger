import BaseController from '../../../infrastructure/Controllers/BaseController';
import Service from '../Services/ConversationService';
import authService from '../../Auth/Services/AuthService';

class ConversationController extends BaseController {
  constructor() {
    super();
    this.service = Service.getService();
  }

  groupAddMethod(req, res) {
    const data = req.body;
    data.users = JSON.parse(data.users);
    data.users.push(req.session.user.id);
    this.service.groupAddService(data);
    res.json(req.session.user);
  }

  async getLatestActivity(req, res, next) {
    const latestActivity = await this.service.getLatestActivity(req.session.user)
      if (latestActivity) {
        res.redirect("/conversations/" + latestActivity._id + "");
      } else next();
  }

  async showGroupChat(req, res, next) {
    const idUser = req.session.user.id;
    const groupChats = await this.service.showGroupChat(req.session.user);
    groupChats.forEach((groupChats) => {
      if (groupChats.member.length == 2) {
        if (groupChats.member[0] == idUser) {
          groupChats.groupName = groupChats.user_2;
        }
        else {
          groupChats.groupName = groupChats.user_1;
        }
      }
    });
    res.locals.groupChats = groupChats;
    next();
  }

  async saveMess(req, res) {
    const data = req.body;
    await this.service.saveMess(data);
    res.json(data);
  }

  async getMessRoom(req, res) {
    const data = req.body;
    const messRoom = await this.service.getMessRoom(data);
    res.json(messRoom);
  }
}

export default ConversationController;
