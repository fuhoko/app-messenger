import socketIo from 'socket.io';
import ConversasionService from '../app/Conversation/Services/ConversationService';
// import FriendService from '../app/Friends/Services/FriendsService';

function socketConfig(server) {
    const io = socketIo(server);
    ConversasionService.io = io.of('/conversations').on('connection', (socket) => {
        socket.on('joinRoom', (idRoom, idUser) => {
            socket.join([idRoom, idUser]);
        });

        socket.on('leaveRoom', (oldRoom) => {
            socket.leave(oldRoom);
        })
    });
}

export default socketConfig;
