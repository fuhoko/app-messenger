import express from 'express';
import ConversationController from '../Controllers/ConversationController';

const router = express.Router();
// Declare Object
const conversationcontroller = new ConversationController();

router.post('/group-add', conversationcontroller.callMethod('groupAddMethod'));

router.post('/save-mess', conversationcontroller.callMethod('saveMess'));

router.post('/get-mess-room', conversationcontroller.callMethod('getMessRoom'))
export default router;
