import BaseRepository from '../../../infrastructure/Repositories/BaseRepository';
import model from '../../../database/model/index';

class AuthRepository extends BaseRepository {
  static repository;

  static getRepository() {
    if (!this.repository) {
      this.repository = new this();
    }

    return this.repository;
  }

  getTableName() {
    return 'users';
  }

  registerByPhone(data) {
    return this.create({
      firstName: data.firstName,
      lastName: data.lastName,
      avatar: 'unavailable',
      phoneNumber: data.phoneNumber,
      email: 'unavailable',
      password: 'unavailable',
      city: 'unavailable',
      describe: 'unavailable',
    });
  }

  registerByEmail(data) {
    return this.create({
      firstName: data.firstName,
      lastName: data.lastName,
      avatar: 'unavailable',
      phoneNumber: 'unavailable',
      email: data.email,
      password: data.password,
      city: 'unavailable',
      describe: 'unavailable',
    });
  }

  getProfileByPhone(data) {
    return this.getBy({
      phoneNumber: data.phoneNumber,
    });
  }

  getProfileByEmail(data) {
    return this.getBy({
      email: data.email,
    });
  }

  editProfile(data, idUser) {
    return this.update({
      id: idUser,
    }, data)
  }

  findUserByEmail(data) {
    return this.getBy({
      email: data.link,
    });
  }

  findUserByPhone(data) {
    return this.getBy({
      phoneNumber: data.link,
    });
  }

  getUserInfo(data) {
    return model.Users.find({
      fullName: { $regex: data.fullName, $options: 'i' },
    })
  }

  findUserOnMongo(data) {
    if (data.id) {
      return model.Users.findOne({
        id: data.id,
      });
    }

    if (data.idFriend) {
      return model.Users.findOne({
        id: data.idFriend,
      });
    }
  }

  updateUserToMongo(data) {
    model.Users.create({
      id: data.id,
      avatar: data.avatar,
      fullName: data.firstName + ' ' + data.lastName,
    });
  }

}

export default AuthRepository;
