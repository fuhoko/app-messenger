import mongoose from 'mongoose';

const { Schema } = mongoose;

const usersSchema = new Schema({
    id: Number,
    avatar: String,
    fullName: String,
    },
    {
        timestamps: true,
    });

export default mongoose.model('Users', usersSchema);
