import BaseRepository from '../../../infrastructure/Repositories/BaseRepository';

import model from '../../../database/model/index';

class ConversationRepository extends BaseRepository {
  static repository;

  static getRepository() {
    if (!this.repository) {
      this.repository = new this();
    }
    return this.repository;
  }

  getTableName() {
    return 'conversations';
  }

  groupAdd(data) {
    model.Conversation.create({
      groupName: data.groupName,
      member: data.users,
      description: data.description,
    });
  }

  getLatestActivity(data) {
    return model.Conversation.findOne({
      member: data.id,
    }).sort({
      updatedAt: -1,
    });
  }

  RoomChat(memberGroup) {
    return model.Conversation.find({
      member: {
        $size: 2,
        $all: memberGroup,
      },
    });
  }

  addFriendChat(memberGroup, infoFriend, infoUser) {
    model.Conversation.create({
      member: memberGroup,
      user_1: infoFriend.fullName,
      user_2: infoUser.fullName,
    });
  }

  showGroupChat(user) {
    return model.Conversation.find({
      member: user.id,
    }).sort({
      updatedAt: -1,
    });
  }

  async saveMess(data) {
    await model.Message.create({
      contentMessage: data.contentMessage,
      nameSender: data.nameSender,
      conversationId: data.idRoom,
      idSender: data.idSender,
    });
    await model.Conversation.updateOne(
      {
        _id: data.idRoom,
      },
      {
        lastNameSender: data.nameSender,
        lastIdSender: data.idSender,
        lastContentMessage: data.contentMessage,
      });
  }

  infoRoom(data) {
    return model.Conversation.findOne({
      _id: data.idRoom,
    })
  }

  getMessRoom(data) {
    return model.Message.find({
      conversationId: data.idRoom,
    }).populate('conversationId');
  }
}

export default ConversationRepository;
