import knex from '../../database/connection';

class BaseRepository {
  constructor() {
    this.tableName = this.getTableName();
  }

  cloneQuery() {
    return knex(this.tableName).clone();
  }

  list(columns = ['*']) {
    return this.cloneQuery().select(columns);
  }

  listBy(clauses = {}, columns = ['*']) {
    return this.cloneQuery().where(clauses).select(columns);
  }

  listJoinBy(clauses = {}, tableConnect, object1, object2, columns = ['*']) {
    return this.cloneQuery().where(clauses).select(columns).join(tableConnect, object1, object2);
  }

  count() {
    return this.cloneQuery().count();
  }

  countBy(clauses = {}) {
    return this.cloneQuery().where(clauses).count();
  }

  getBy(clauses = {}, columns = ['*']) {
    return this.cloneQuery().where(clauses).select(columns).first();
  }

  create(attributes, trx, returning = ['*']) {
    return this.cloneQuery().insert(attributes).returning(returning);
  }

  update(clauses, attributes, returning = ['*']) {
    return this.cloneQuery().where(clauses).update(attributes).returning(returning);
  }

  updateOrWhere(clauses1, clauses2, attributes, returning = ['*']) {
    return this.cloneQuery().where(clauses1).orWhere(clauses2).update(attributes).returning(returning);
  }

  delete(clauses) {
    return this.cloneQuery().where(clauses).delete();
  }

  deleteOrWhere(clauses1, clauses2) {
    return this.cloneQuery().where(clauses1).orWhere(clauses2).delete();
  }
}

export default BaseRepository;
