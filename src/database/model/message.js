import mongoose from 'mongoose';

const { Schema } = mongoose;

const messageSchema = new Schema({
    nameSender: {
        type: String,
        required: true,
        trim: true,
    },
    conversationId: {
        type: Schema.Types.ObjectId,
        ref: 'Conversation',
    },
    idSender: Number,
    contentMessage: {
        type: String,
        trim: true,
    },
    },
    {
        timestamps: true,
    });

export default mongoose.model('Message', messageSchema);
