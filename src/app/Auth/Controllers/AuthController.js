import BaseController from '../../../infrastructure/Controllers/BaseController';
import authservice from '../Services/AuthService';

class AuthController extends BaseController {
  constructor() {
    super();
    this.authservice = authservice.getService();
  }

  async registerByPhone(req, res, next) {
    const data = req.body;
    await this.authservice.registerByPhone(data);
    return next();
  }

  async registerByEmail(req) {
    const data = req.body;
    await this.authservice.registerByEmail(data);
  }

  logout(req, res) {
    delete req.session.user;
    delete req.session.uid;
    return res.redirect('/loginphone');
  }

  getProfileByPhone(req, res) {
    const data = req.body;
    this.authservice.getProfileByPhone(data, req);
    res.json({ verifyCode: true });
  }

  async getProfileByEmail(req, res) {
    const data = req.body;
    const user = await this.authservice.getProfileByEmail(data);
    if (user.email == data.email && user.password == data.password) {
      req.session.user = user;
      req.session.save();
      return res.json('true');
    }
    res.json('false');
  }

  async editProfile(req, res) {
    const data = req.body;
    const uid = req.session.uid;
    const idUser = req.session.user.id;

    const error = await this.authservice.editProfile(data, uid, idUser);
    if (error === 'true') {
      return res.json('true');
    }
    res.json('false');
  }

  // async uploadavatar(req, res) {
  //   const data = req.body;
  // }

  async getUserInfo(req, res) {
    const data = req.body;
    const result = await this.authservice.getUserInfo(data);
    for (let i = 0; i < result.length; i += 1) {
      if (result[i].id == req.session.user.id) {
        result.splice(i, 1);
      }
    }
    return res.json(result);
  } 

  async updateUserToMongo(req, res, next) {
    await this.authservice.updateUserToMongo(req.session.user);
    next();
  }

}

export default AuthController;
