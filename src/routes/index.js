import express from 'express';
import authRouter from '../app/Auth/Routes/routes';
import friendRouter from '../app/Friends/Routes/routes';
import conversation from '../app/Conversation/Routes/routes';
import AuthController from '../app/Auth/Controllers/AuthController';
import FriendsController from '../app/Friends/Controllers/FriendsController';
import ConversationController from '../app/Conversation/Controllers/ConversationController';

const router = express.Router();
const authController = new AuthController();
const friendsController = new FriendsController();
const conversationController = new ConversationController();

router.use(authRouter);

router.use(friendRouter);

router.use(conversation);

router.get('/conversations/home', conversationController.callMethod('getLatestActivity'));

router.get('/conversations/:slug', authController.callMethod('updateUserToMongo'), conversationController.callMethod('showGroupChat'), friendsController.callMethod('showFriend'), friendsController.callMethod('friendRequest'));

export default router;
