import express from 'express';
import Controller from '../Controllers/AuthController';
import Middleware from '../Middlewares/AuthMiddleware';
import multer from '../../../config/multer';

const router = express.Router();
const controller = new Controller();
const middleware = new Middleware();

router.route('/login')
  .get((req, res) => res.render('app/login'))
  .post(controller.callMethod('getProfileByEmail'));

router.route('/registerphone')
  .get((req, res) => res.render('app/register-phone'))
  .post(middleware.verifyCode, controller.callMethod('registerByPhone'), controller.callMethod('getProfileByPhone'));

router.route('/loginphone')
  .get((req, res) => res.render('app/login-phone'))
  .post(middleware.verifyCode, controller.callMethod('getProfileByPhone'));

router.route('/registeremail')
  .get((req, res) => res.render('app/register-email'))
  .post(controller.callMethod('registerByEmail'));

router.route('/editprofile').post(controller.callMethod('editProfile'));

// router.post('/uploadavatar', multer.uploadImages, controller.callMethod('uploadavatar'));

router.get('/logout', controller.logout);

router.get('/resetpassword', (req, res) => res.render('app/reset-password'));

router.post('/get-user-info', controller.callMethod('getUserInfo'));

export default router;
