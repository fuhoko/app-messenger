import createError from 'http-errors';
import express from 'express';
import path from 'path';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
// import logger from 'morgan';
import 'dotenv/config';
import session from 'express-session';
import mongoose from 'mongoose';

import initRoutes from './config/routes';

import socket from './config/socket';
import model from './database/model';

const PgSession = require('connect-pg-simple')(session);
const SessionPool = require('pg').Pool;

const app = express();
const server = require('http').createServer(app);

// mongodb
mongoose.connect('mongodb://localhost/app-chat', { useNewUrlParser: true, useUnifiedTopology: true });

socket(server);

// session
const sessionDBaccess = new SessionPool({
  user: process.env.DATABASE_USER,
  password: process.env.DATABASE_PASSWORD,
  database: process.env.DATABASE_DB,
});
const sessionConfig = {
  store: new PgSession({
      pool: sessionDBaccess,
      tableName: 'session',
  }),
  name: 'SID',
  secret: 'yowtf',
  resave: false,
  saveUninitialized: true,
  cookie: {
      maxAge: 1000 * 60 * 60 * 24 * 7,
      aameSite: true,
      secure: false,
  },
};
app.use(session(sessionConfig));

// view engine setup
app.set('views', path.join(__dirname, 'resources/views'));
app.set('view engine', 'pug');

// app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

initRoutes(app);

// catch 404 and forward to error handler
// app.use((req, res, next) => {
//   next(createError(404));
// });

// // error handler
// app.use((err, req, res) => {
//   // set locals, only providing error in development
//   res.locals.message = err.message;
//   res.locals.error = req.app.get('env') === 'development' ? err : {};

//   // render the error page
//   res.status(err.status || 500);
//   res.render('error');
// });

// export default app;

export default { app, server };
