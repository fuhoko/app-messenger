import Repository from '../Repositories/AuthRepository';
import admin from '../../../config/firebase';

class AuthService {
  static service;

  constructor() {
    this.repository = Repository.getRepository();
  }

  static getService() {
    if (!this.service) {
      this.service = new this();
    }
    return this.service;
  }

  async verifyCode(data, req) {
    await admin.auth().verifyIdToken(data.idToken)
      .then((decodedToken) => {
        req.session.uid = decodedToken.uid;
        req.session.save();
      });
  }

  verifyAuthentication(req, res, next) {
    if (req.session.user) {
      return next();
    }
    return res.redirect('/login');
  }

  verifyNotAuthtication(req, res, next) {
    if (!req.session.user) {
      return next();
    }
    return res.redirect('/conversations');
  }

  async registerByPhone(data) {
    await this.repository.registerByPhone(data);
  }

  async registerByEmail(data) {
    await this.repository.registerByEmail(data);
  }

  async getProfileByPhone(data, req) {
    const user = await this.repository.getProfileByPhone(data);
    req.session.user = user;
    await req.session.save();
  }

  async getProfileByEmail(data) {
    return this.repository.getProfileByEmail(data);
  }

  async editProfile(data, uid, idUser) {
    const catchError = await admin.auth().updateUser(uid, {
      phoneNumber: data.phoneNumber,
      displayName: data.firstName + ' ' + data.lastName,
      email: data.email,
      password: data.password,
      emailVerified: false,
      }).then(() => {
        return 'false';
      })
      .catch(() => {
        return 'true';
      });
      if (catchError == 'false') {
        await this.repository.editProfile(data, idUser)
      }
    return catchError;
  }

  async getUserInfo(data) {
    const result = await this.repository.getUserInfo(data);
    if (!result) {
      return 'error';
    }
    return result;
  }

  async updateUserToMongo(data) {
    const user = await this.repository.findUserOnMongo(data);
    console.log("TCL: AuthService -> updateUserToMongo -> user", user);
    if (!user) {
      await this.repository.updateUserToMongo(data);
    }
  }

  async findUserByEmail(data) {
    const infoFriend = await this.repository.findUserByEmail(data);
    return infoFriend;
  }

  async findUserByPhone(data) {
    return await this.repository.findUserByPhone(data);
  }

  async findUserById(data) {
    return await this.repository.findUserOnMongo(data);
  }
}
export default AuthService;
