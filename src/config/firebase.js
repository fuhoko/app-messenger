import admin from 'firebase-admin';
import serviceAccount from '../../messenger-demo-556e8-firebase-adminsdk-qy0sa-39834a4927.json';

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: 'https://messenger-demo-556e8.firebaseio.com',
});

export default admin;
