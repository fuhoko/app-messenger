import BaseRepository from '../../../infrastructure/Repositories/BaseRepository';

class FriendsRepository extends BaseRepository {
  static friendRepository;

  static getfriendRepository() {
    if (!this.friendRepository) {
      this.friendRepository = new this();
    }
    return this.friendRepository;
  }

  getTableName() {
    return 'friends';
  }

  checkFriend(idFriend, idUser) {
    return this.getBy({
      user_1: idUser,
      user_2: idFriend,
    })
  }

  async addfriend(infoFriend, idUser) {
    await this.create({
      user_1: idUser,
      user_2: infoFriend.id,
      id_receiver: infoFriend.id,
      status: 0,
    });
    return this.create({
      user_1: infoFriend.id,
      user_2: idUser,
      id_receiver: infoFriend.id,
      status: 0,
    });
  }

  friendRequest(idUser) {
    return this.listJoinBy({
      id_receiver: idUser,
      user_1: idUser,
      status: 0,
    }, 'users', 'user_2', 'users.id');
  }

  showFriend(idUser) {
    return this.listJoinBy({
      user_1: idUser,
      status: 1,
    }, 'users', 'user_2', 'users.id');
  }

  acceptFriend(data, idUser) {
    return this.updateOrWhere({
      user_1: idUser,
      user_2: data.idFriend,
    }, {
      user_1: data.idFriend,
      user_2: idUser,
    }, {
      status: 1,
    });
  }

  denyFriend(data, idUser) {
    console.log(data)
    return this.deleteOrWhere({
      user_1: idUser,
      user_2: data.idFriend,
    }, {
      user_1: data.idFriend,
      user_2: idUser,
    });
  }
}

export default FriendsRepository;
