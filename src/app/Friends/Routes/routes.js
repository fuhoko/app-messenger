import express from 'express';
import Controller from '../Controllers/FriendsController';
import Middleware from '../Middlewares/FriendsMiddleware';

const router = express.Router();
const controller = new Controller();
const middleware = new Middleware();

router.route('/addfriend').post(controller.callMethod('addfriend'));

router.post('/acceptfriend', controller.callMethod('acceptFriend'));

router.post('/denyfriend', controller.callMethod('denyFriend'));

export default router;
