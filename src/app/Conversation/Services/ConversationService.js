import ConversationRepository from '../Repositories/ConversationRepository';

class ConversationService {
  static service;
  static io;

  constructor() {
    this.repository = ConversationRepository.getRepository();
  }

  static getService() {
    if (!this.service) {
      this.service = new this();
    }
    return this.service;
  }

  async groupAddService(data) {
    await this.repository.groupAdd(data);
  }

  async getLatestActivity(data) {
    return await this.repository.getLatestActivity(data);
  }

  async RoomChat(memberGroup) {
    return await this.repository.RoomChat(memberGroup);
  }

  async addFriendChat(memberGroup, infoFriend, infoUser) {
    return await this.repository.addFriendChat(memberGroup, infoFriend, infoUser)
  }

  async showGroupChat(user) {
    return await this.repository.showGroupChat(user);
  }

  async saveMess(data) {
    ConversationService.io.to(data.idRoom).emit('show-message', data);
    const infoRoom = await this.repository.infoRoom(data);

    infoRoom.member.forEach((member) => {
      ConversationService.io.to(member).emit('reload-chatbar', data);
    });

    await this.repository.saveMess(data);
  }

  async getMessRoom(data) {
    return await this.repository.getMessRoom(data);
  }
}

export default ConversationService;
